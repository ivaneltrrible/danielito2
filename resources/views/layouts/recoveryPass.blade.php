<!DOCTYPE html>
<html lang="es">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Netbox Soluciones</title>
        <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.ico') }}">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/_all-skins.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/jquery.dataTables.min.css')}}" />
        <link rel="stylesheet" href="{{asset('public/assets/css/styles.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/circle-buttons.css')}}">
        <!-- <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.min.css')}}"> -->
        <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap-datetimepicker.min.css')}}">
	</head>
	<body id="login">
        <div class="col-lg-12">
            <section class="body-sign">
			    <div class="center-sign">
                    @section('cuerpo')
                    @show    
                </div>
            </section>
        <div>
    </body>
</html>