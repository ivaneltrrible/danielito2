@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-columns"></i> Comparador</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    <div class="row margin-bottom-20">
                        <div class="col-xs-3 col-md-3 col-lg-3">
                            <label>Columna 1</label>
                            {!! Form::textarea('column1', null, ['id'=>'array1', 'class'=>'form-control' , 'rows' => '25']) !!}
                        </div>
                        <div class="col-xs-3 col-md-3 col-lg-3">
                            <label>Columna 2</label>
                            {!! Form::textarea('column2', null, ['id'=>'array2', 'class'=>'form-control' , 'rows' => '25']) !!}
                        </div>
                        <div class="col-xs-3 col-md-3 col-lg-3">
                        <br><br><br><br><br><br><br><br><center><button type="button" name="" onclick="comp();" class="btn btn-primary btn-lg" data-dismiss="modal"><i class="fa fa-chevron-right"></i></button></center><br><br><br><br><br><br>
                            <center><button type="button" name="" onclick="refresh();" class="btn btn-primary btn-lg" data-dismiss="modal"><i class="fa fa-refresh"></i></button></center>
                        </div>
                        <div class="col-xs-3 col-md-3 col-lg-3">
                            <label>Resultado</label>
                            {!! Form::textarea('result', null, ['id'=>'array3', 'class'=>'form-control' , 'rows' => '25']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@section('script')
<script type="text/javascript">


    
    function comp(){
        var array1 = $("#array1").val();
        array1 = array1.split("\n");
        var array2 = $("#array2").val();
        array2 = array2.split("\n");
        
        var array3=new Array();

        for(var i=0;i<array1.length;i++)
        {
            for(var j=0;j<array1.length;j++)
            {
                if(array1[i]==array2[j])
                    array3.push(array1[i]);
            }
        }
        var result = array3.toString();
        var re = /,/g;
        $("#array3").val(result.replace(re,"\n"));
    }

    function refresh(){
        $("#array1").val("");
        $("#array2").val("");
        $("#array3").val("");
    }
    

</script>

@endsection



