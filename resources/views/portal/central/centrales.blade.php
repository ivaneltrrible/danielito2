@extends('layouts.portal')


@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-building"></i> Centrales</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                @if(Gate::allows('Root'))
                <div class="panel-body">
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="centralesRoot">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>CLIENTE</th>
                                <th>NOMBRE</th>
                                <th>IDENTIFICADOR</th>
                                <th><center>ACCIONES</center></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                @endif
                @if(Gate::allows('Administrador'))
                <div class="panel-body">
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="centralesAdmin">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>NOMBRE</th>
                                <th>IDENTIFICADOR</th>
                                <th><center>ACCIONES</center></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div>
		<div class="action-button-circle" data-toggle="modal">
            <a href="{{ route('centers/add') }}" title="Crear una central"><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>
        </div>
	</div>

    <!-- MODAL ELIMINAR -->
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="deleteForm" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Eliminar Central</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <!-- {{ method_field('DELETE') }} -->
                        <!-- <p class="text-center">¿Está seguro que desea eliminar?</p> -->
                        <div id="contenido"></div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()"><i class="fa fa-trash"></i> Eliminar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#centralesRoot').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getCentrales') }}',
        columns: [
            {data: 'id_central', name: 'centrales.id_central'},
            {data: 'id_customer', name: 'customers.name'},
            {data: 'name', name: 'centrales.name'},
            {data: 'identifier', name: 'centrales.identifier'},
            {"render": function(data, type, row) {
                return "<center><a href='centers/edit/" + row.id_central + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
                <a href='javascript:;' data-toggle='modal' onclick='deleteCentral(\""+row.id_central+"\",\""+row.name+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></a></center>";
            },
            "targets": 4},
        ]
    });

    $('#centralesAdmin').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getCentrales') }}',
        columns: [
            {data: 'id_central', name: 'id_central'},
            // {data: 'id_customer', name: 'id_customer'},
            {data: 'name', name: 'name'},
            {data: 'identifier', name: 'identifier'},
            {"render": function(data, type, row) {
                return "<center><a href='centers/edit/" + row.id_central + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
                <a href='javascript:;' data-toggle='modal' onclick='deleteCentral(\""+row.id_central+"\",\""+row.name+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></a></center>";
            },
            "targets": 4},
        ]
    });
});

function deleteCentral(id,name)
{
    var id = id;
    var url = '{{ route("centers/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append('<p class="text-center">¿Está seguro que desea eliminar la central "'+name+'"?</p>');
}

function formSubmit()
{
    $("#deleteForm").submit();
}
</script>
@endsection



