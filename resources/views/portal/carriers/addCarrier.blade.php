@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Crear Troncal</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <br>    
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'carriers/store', 'method' => 'POST']) !!} 
                    <!-- <div class="panel-heading">Agregar cliente</div> -->
                    <div class="panel-body">
                        <div class="row margin-bottom-20">
                            @if(Gate::allows('Root'))
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <label>Troncal</label>
                                    {!! Form::text('carrier', null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <label>Nombre</label>
                                    {!! Form::text('nombre', null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <label>Usuaio</label>
                                    {!! Form::select('user', ['' => 'Selecciona un usuario']+$user_list, null, ['class' => 'form-control']) !!}
                                </div>
                            @endif
                            @if(Gate::allows('Administrador'))
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Troncal</label>
                                    {!! Form::text('carrier', null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Nombre</label>
                                    {!! Form::text('nombre', null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hide">
                                    <label>Usuaio</label>
                                    {!! Form::text('user', auth()->user()->id_user, ['class'=>'form-control', 'readonly'=>'readonly']) !!}
                                </div>
                            @endif
                        </div>
                        <br>
                        <div class="row">
                            <!-- <div class="col-md-offset-10 col-md-12"> -->
                            <div align="right">
                                <a href="{!! route('carriers') !!}">
                                    <button class="btn btn-secondary" type="button"><i class="fa fa-undo"></i> Regresar</button>
                                </a>
                                <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {

});
</script>
@endsection