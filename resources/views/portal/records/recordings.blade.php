@extends('layouts.portal')


@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-file-audio-o"></i> Grabaciones</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                <div class="panel-body">
                    <button type="submit" data-toggle='modal' data-target='#filter' id="filtrar" class="btn btn-primary"><i class="fa fa-filter"></i> Filtrar</button>
                    <br><br>   
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="grabaciones">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>CLIENTE</th>
                                <th>CENTRAL</th>
                                <th>SERVIDOR</th>
                                <th>NOMBRE</th>
                                <th>FECHA</th>
                                <th>TAMAÑO</th>
                                <th>DURACION</th>
                                <!-- <th>RUTA</th> -->
                                <th>TIPO</th>
                                <th><center>ACCIONES</center></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL FILTRAR -->
<div id="filter" class="modal fade text-primary" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <form action="" id="search_filter" method="get">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Filtrar por:</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="row margin-bottom-20">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label>Cliente:</label>
                            {!! Form::select('client', [""=>"Selecciona un cliente:"]+$list_clients, null,['class'=>'form-control']) !!}
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label>Central:</label>
                            {!! Form::select('central', [""=>"Selecciona una central:"]+$list_centrales, null,['class'=>'form-control']) !!}
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label>Servidor:</label>
                            {!! Form::select('server',[""=>"Selecciona un servidor:"]+$list_servers, null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <br>
                    <div class="row margin-bottom-20">
                        <div class="col-md-6 col-md-6 col-sm-12 col-xs-12">
                            <span>
                                <label>Fechas - De:</label>
                                <div class='input-group date' id='fecha_inicio'>
                                    {!! Form::text('start_date', $now, ['class'=>'form-control']) !!}    
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </span> 
                        </div>
                        <div class="col-md-6 col-md-6 col-sm-12 col-xs-12">
                            <span>
                                <label>A:</label>
                                <div class='input-group date' id='fecha_fin'>
                                    {!! Form::text('finish_date', $tomorrow, ['class'=>'form-control']) !!}    
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </span> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                        <button type="submit" name="" id="btn-filter" class="btn btn-primary" data-dismiss="modal" ><i class="fa fa-filter"></i> Filtrar</button>
                    </center>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- MODAL PLAY -->
<div class="modal fade" id="playModal" tabindex="10" role="dialog" aria-labelledby="playModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header bg-primary">
            <h3 class="modal-title" id="playModalLabel">Reproductor de audio</h3>
        </div>
        <div class="modal-body">
            <div id="reproductor">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#fecha_inicio').datetimepicker({
        format: 'yyyy-mm-dd',
        language:  'es',
        minViewMode: 2,
        // daysOfWeekDisabled: [0,6],
        weekStart: 1,
        // todayBtn:  1,
        autoclose: 1,
        // todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        // showMeridian: 1,
        // minDate: '2019-02-13',
        minView: 2,
    });

    $('#fecha_fin').datetimepicker({
        format: 'yyyy-mm-dd',
        language:  'es',
        minViewMode: 2,
        // daysOfWeekDisabled: [0,6],
        weekStart: 1,
        // todayBtn:  1,
        autoclose: 1,
        // todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        // showMeridian: 1,
        // minDate: '2019-02-13',
        minView: 2,
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#grabaciones').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: {
            url: '{{ route('datatable/getRecordings') }}',
            type: 'GET',
            data: function (d) {
                d.client = $('select[name=client]').val();
                d.central = $('select[name=central]').val();
                d.server = $('select[name=server]').val();
                d.start_date = $('input[name=start_date]').val();
                d.finish_date = $('input[name=finish_date]').val();
            }
        },
        columns: [
            {data: 'id_record', name: 'records.id_record'},
            {data: 'name_customer', name: 'name_customer'},
            {data: 'name_central', name: 'name_central'},
            {data: 'name_server', name: 'name_server'},
            {data: 'name', name: 'records.name'},
            {data: 'date', name: 'records.date'},
            {data: 'size', name: 'records.size'},
            {data: 'time', name: 'records.time'},
            // {data: 'route', name: 'records.route'},
            {data: 'type', name: 'records.type'},
            {"render": function(data, type, row) {
                return "<center><a onclick=\"PlayAudio('"+row.id_record+"','"+row.date+"')\" title=\"Play\"><i class='fa fa-play iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
                <a href='recordings/download/" + row.id_record + "' title=\"Descargar\"><i class='fa fa-download iconos'></i></a></center>";
            },
            "targets": 9},
        ]
    });

    $('#btn-filter').click(function(){
        // tableReport.reload();
        $('#grabaciones').DataTable().ajax.reload();
    });
});
function PlayAudio(grabacion, fecha){
    $.get("/recordings/play/" + grabacion, function ( data ) {
        $('#reproductor').empty();
        $('#reproductor').append("<center><em class='fa fa-music fa-5x'></em></center>");
        $('#reproductor').append("<br>");
        $('#reproductor').append("<center><span><strong>Audio</strong>: <i>"+data.name+"</i><br><strong>Fecha</strong>: <i>"+fecha+"</i><br></span></center>");
        $('#reproductor').append("<br>");
        var ruta_mal = data.route;
        var ruta = ruta_mal.replace("/var/spool/asterisk/monitor/", "https://45.63.3.215:13123/RECORDINGS/");
        $('#reproductor').append("<center><audio id='playAudio' src='"+ruta+"' controls></audio></center>");
        $('#playModal').modal('show');
    });
}
</script>
@endsection



