<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Datatables;
use Redirect;
use App\User;
use App\Rol;
use App\CarrierLog;
use App\Http\Repositories;
use App\Http\Repositories\UserRepo;
use App\Http\Repositories\RolRepo;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;

class HistoryController extends Controller
{
    protected $UserRepo, $RolRepo;
    
    public function __construct(UserRepo $UserRepo, RolRepo $RolRepo)
    {
        $this->UserRepo = $UserRepo;
        $this->RolRepo = $RolRepo;
    }

    public function index()
    { 
        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            return view('portal.history.history');     
        }
        abort(404);
    }

    public function getHistory()
    {
        if(Gate::allows('Root'))
        {
            return DataTables::of(CarrierLog::query())->make(true);
        }
        if(Gate::allows('Administrador'))
        {
            $history = DB::table('carriers_log')
            ->select(['carriers_log.acli','carriers_log.ncli','carriers_log.date','users.user',DB::raw('CONCAT(carriers.carrier, " - ", carriers.nombre) AS carrier') ])
            ->join('users', 'users.id_user', '=', 'carriers_log.user')
            ->join('carriers', 'carriers.id_carrier', '=', 'carriers_log.id_carrier')
            ->where('carriers_log.user','=',Auth::user()->id_user);
            return DataTables::of($history)->make(true);
        }
        
    }
}