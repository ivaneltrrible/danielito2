<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TarifaRequest;
use Datatables;
use Redirect;
use App\User;
use App\Rol;
use App\Tarifa;
use App\Http\Repositories;
use App\Http\Repositories\TarifaRepo;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;

class TarifaController extends Controller
{
    protected $TarifaRepo;
    
    public function __construct(TarifaRepo $TarifaRepo)
    {
        $this->TarifaRepo = $TarifaRepo;
    }

    public function index()
    { 
        if(Gate::allows('Root'))
        {
            return view('portal.tarifas.tarifas');     
        }
        abort(404);
    }
    
    public function index_internacional()
    { 
        if(Gate::allows('Root') || Gate::allows('Tarifas'))
        {
            $paises = DB::table('tarifa_internacional')->select(['pais'])->groupBy('pais')->orderBy('pais', 'asc')->get();
            return view('portal.tarifas.tarifa_internacional', compact('paises'));     
        }
        abort(404);
    }

    public function getTarifas()
    {
        if(Gate::allows('Root'))
        {
            
            $tarifas = DB::table('tarifas')
            ->select([
                'prefijo', 'description','intervalo_n','intervalo_1','precio'
            ]);
            return DataTables::of($tarifas)->make(true);
        }
    }

    public function storeTarifa(TarifaRequest $request)
    {
        $tarifa = $this->TarifaRepo->storeTarifa($request->all());
        if($tarifa){
            Session::flash('message-success', 'Nodo '.$request['prefijo'].' agregado.');   
            return redirect()->route('tarifas');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al registrar el nodo');            
            return redirect()->back()->withInput(); 
        }
    }

    // public function editTarifa($id)
    // {
    //     if(Gate::allows('Root'))
    //     {
    //         $carrier = $this->CarriersRepo->getCarrier($id);
    //         return View("portal.carriers.editCarrier", compact('carrier','user_list')); 
    //     }
    //     abort(404);
    // }

    public function updateTarifa(CarrierRequest $request, $id)
    {
        $carrier = $this->CarriersRepo->updateCarrier($request->all(), $id);
        if($carrier)
        {            
            Session::flash('message-success', 'Troncal actualizada.');
            return redirect()->route('carriers');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al actualizar la troncal '.$id.' ');            
            return redirect()->route('carriers'); 
        }
    }

    public function deleteTarifa($id)
    {
        if(Gate::allows('Root'))
        {
            $tarifa = $this->TarifaRepo->deleteTarifa($id);
            if($tarifa)
            {
                Session::flash('message-success', 'Nodo eliminada.');  
                return redirect()->route('tarifas');
            }else{
                Session::flash('message-danger', 'Ocurrió un error al eliminar el nodo '.$id.'.'); 
                return redirect()->route('tarifas');
            }  
        }
    }

    public function updatePrices(Request $request)
    {
        if(Gate::allows('Root'))
        {
            // $movil = Tarifa::where('description', 'LIKE', '%Mobile Telcel%')->update(['precio' => $request['movil']]);
            // $otros = Tarifa::where('description', 'NOT LIKE', '%Mobile Telcel%')->update(['precio' => $request['otros']]);
            // $fijo = Tarifa::where('description', 'NOT LIKE', '%Mobile%')->update(['precio' => $request['fijo']]);

            $query = Tarifa::select(DB::raw('count(*) as total'))->get();
            foreach($query as $total)
            {
                $parte = $total->total / 4;
            }
            $parte1 = round($parte);
    
            $tarifa1 = Tarifa::skip(0)->take($parte1)->get();
            $excel1 = Excel::create('nodos_parte1_'.Carbon::now(), function($excel) use ($tarifa1, $request) {
                $excel->sheet('nodos_parte1_', function($sheet) use ($tarifa1, $request) {
                    $sheet->cell('A1', function($cell) {$cell->setValue('Prefijo'); });
                    $sheet->cell('B1', function($cell) {$cell->setValue('Description'); });
                    $sheet->cell('C1', function($cell) {$cell->setValue('Intervalo N'); });
                    $sheet->cell('D1', function($cell) {$cell->setValue('Intervalo 1'); });
                    $sheet->cell('E1', function($cell) {$cell->setValue('Precio'); });
                 
                    foreach ($tarifa1 as $key => $value){
                        $i = $key+2;
                        $sheet->cell('A'.$i, $value['prefijo']); 
                        $sheet->cell('B'.$i, $value['description']); 
                        $sheet->cell('C'.$i, $value['intervalo_n']); 
                        $sheet->cell('D'.$i, $value['intervalo_n']); 
                        if(!strstr($value['description'], 'Telcel'))
                        {
                            $sheet->cell('E'.$i, $request['otros']);  
                        }
                        if(strstr($value['description'], 'Telcel'))
                        {
                            $sheet->cell('E'.$i, $request['movil']); 
                        }
                        if(!strstr($value['description'], 'Mobile'))
                        {
                            $sheet->cell('E'.$i, $request['fijo']);  
                        }
                        
                    }  
                });
            })->download('csv');
            // })->store('csv', storage_path('excel/exports'));

            $tarifa2 = Tarifa::skip($parte1)->take($parte1)->get();
            $excel2 = Excel::create('nodos_parte2_'.Carbon::now(), function($excel) use ($tarifa2, $request) {
                $excel->sheet('nodos_parte2_', function($sheet) use ($tarifa2, $request) {
                    $sheet->cell('A1', function($cell) {$cell->setValue('Prefijo'); });
                    $sheet->cell('B1', function($cell) {$cell->setValue('Description'); });
                    $sheet->cell('C1', function($cell) {$cell->setValue('Intervalo N'); });
                    $sheet->cell('D1', function($cell) {$cell->setValue('Intervalo 1'); });
                    $sheet->cell('E1', function($cell) {$cell->setValue('Precio'); });
                
                    foreach ($tarifa2 as $key => $value){
                        $i = $key+2;
                        $sheet->cell('A'.$i, $value['prefijo']); 
                        $sheet->cell('B'.$i, $value['description']); 
                        $sheet->cell('C'.$i, $value['intervalo_n']); 
                        $sheet->cell('D'.$i, $value['intervalo_n']); 
                        if(!strstr($value['description'], 'Telcel'))
                        {
                            $sheet->cell('E'.$i, $request['otros']);  
                        }
                        if(strstr($value['description'], 'Telcel'))
                        {
                            $sheet->cell('E'.$i, $request['movil']); 
                        }
                        if(!strstr($value['description'], 'Mobile'))
                        {
                            $sheet->cell('E'.$i, $request['fijo']);  
                        }
                    }  
                });
            })->download('csv');
           
            if($excel1 && $excel2)
            {
                Session::flash('message-success', 'Tarifa actualizada.');  
                return redirect()->route('tarifas');
            }else{
                Session::flash('message-danger', 'Ocurrió un error al actualizar los precios'); 
                return redirect()->route('tarifas');
            }  
        }
    }

    public function exportTarifa1(Request $request)
    {
        $query = Tarifa::select(DB::raw('count(*) as total'))->get();
        foreach($query as $total)
        {
            $parte = $total->total / 4;
        }
        $parte1 = round($parte);

        $did = '0.00';

        $tarifa1 = Tarifa::skip(0)->take($parte1)->get();
        $excel1 = Excel::create('nodos_parte1_'.Carbon::now(), function($excel) use ($tarifa1, $request, $did) {
            $excel->sheet('nodos_parte1_', function($sheet) use ($tarifa1, $request, $did) {
                $sheet->cell('A1', function($cell) {$cell->setValue('Prefijo'); });
                $sheet->cell('B1', function($cell) {$cell->setValue('Description'); });
                $sheet->cell('C1', function($cell) {$cell->setValue('Intervalo N'); });
                $sheet->cell('D1', function($cell) {$cell->setValue('Intervalo 1'); });
                $sheet->cell('E1', function($cell) {$cell->setValue('Precio'); });
             
                foreach ($tarifa1 as $key => $value){
                    $i = $key+2;
                    $sheet->cell('A'.$i, $value['prefijo']); 
                    $sheet->cell('B'.$i, $value['description']); 
                    $sheet->cell('C'.$i, $value['intervalo_n']); 
                    $sheet->cell('D'.$i, $value['intervalo_n']); 
                    if($value['tipo'] == 'DID')
                    {
                        $sheet->cell('E'.$i, $did);   
                    }
                    if($value['tipo'] == 'FIJO')
                    {
                        $sheet->cell('E'.$i, $request['fijo']); 
                    }
                    if($value['tipo'] == 'MOVIL')
                    {
                        $sheet->cell('E'.$i, $request['movil']);
                    }
                    if($value['tipo'] == 'OTROS')
                    {
                        $sheet->cell('E'.$i, $request['otros']);  
                    }
                    // if(!strstr($value['description'], 'Telcel'))
                    // {
                    //     $sheet->cell('E'.$i, $request['otros']);  
                    // }
                    // if(strstr($value['description'], 'Telcel'))
                    // {
                    //     $sheet->cell('E'.$i, $request['movil']); 
                    // }
                    // if(!strstr($value['description'], 'Mobile'))
                    // {
                    //     $sheet->cell('E'.$i, $request['fijo']);  
                    // }
                    // if(strstr($value['description'], 'DID'))
                    // {
                    //     $sheet->cell('E'.$i, '0'); 
                    // }
                    
                   
                    
                }  
            });
        })->export('csv');
        // })->store('csv', storage_path('excel/exports'));
        
    }

    public function exportTarifa2(Request $request)
    {
        $query = Tarifa::select(DB::raw('count(*) as total'))->get();
        foreach($query as $total)
        {
            $parte = $total->total / 4;
        }
        $parte1 = round($parte);

        $did = '0.00';

        $tarifa2 = Tarifa::skip($parte1)->take($parte1)->get();
        $excel2 = Excel::create('nodos_parte2_'.Carbon::now(), function($excel) use ($tarifa2, $request, $did) {
            $excel->sheet('nodos_parte2_', function($sheet) use ($tarifa2, $request, $did) {
                $sheet->cell('A1', function($cell) {$cell->setValue('Prefijo'); });
                $sheet->cell('B1', function($cell) {$cell->setValue('Description'); });
                $sheet->cell('C1', function($cell) {$cell->setValue('Intervalo N'); });
                $sheet->cell('D1', function($cell) {$cell->setValue('Intervalo 1'); });
                $sheet->cell('E1', function($cell) {$cell->setValue('Precio'); });
            
                foreach ($tarifa2 as $key => $value){
                    $i = $key+2;
                    $sheet->cell('A'.$i, $value['prefijo']); 
                    $sheet->cell('B'.$i, $value['description']); 
                    $sheet->cell('C'.$i, $value['intervalo_n']); 
                    $sheet->cell('D'.$i, $value['intervalo_n']); 
                    if($value['tipo'] == 'DID')
                    {
                        $sheet->cell('E'.$i, $did);     
                    }
                    if($value['tipo'] == 'FIJO')
                    {
                        $sheet->cell('E'.$i, $request['fijo']); 
                    }
                    if($value['tipo'] == 'MOVIL')
                    {
                        $sheet->cell('E'.$i, $request['movil']);
                    }
                    if($value['tipo'] == 'OTROS')
                    {
                        $sheet->cell('E'.$i, $request['otros']);  
                    }
                }  
            });
        })->export('csv');
    }

    public function exportTarifa3(Request $request)
    {
        $query = Tarifa::select(DB::raw('count(*) as total'))->get();
        foreach($query as $total)
        {
            $parte = $total->total / 4;
        }
        $parte1 = round($parte);

        $did = '0.00';

        $tarifa3 = Tarifa::skip($parte1 + $parte1)->take($parte1)->get();
        Excel::create('nodos_parte3_'.Carbon::now(), function($excel) use ($tarifa3, $request, $did) {
            $excel->sheet('nodos_parte3_', function($sheet) use ($tarifa3, $request, $did) {
                $sheet->cell('A1', function($cell) {$cell->setValue('Prefijo'); });
                $sheet->cell('B1', function($cell) {$cell->setValue('Description'); });
                $sheet->cell('C1', function($cell) {$cell->setValue('Intervalo N'); });
                $sheet->cell('D1', function($cell) {$cell->setValue('Intervalo 1'); });
                $sheet->cell('E1', function($cell) {$cell->setValue('Precio'); });
             
                foreach ($tarifa3 as $key => $value){
                    $i = $key+2;
                    $sheet->cell('A'.$i, $value['prefijo']); 
                    $sheet->cell('B'.$i, $value['description']); 
                    $sheet->cell('C'.$i, $value['intervalo_n']); 
                    $sheet->cell('D'.$i, $value['intervalo_n']); 
                    if($value['tipo'] == 'DID')
                    {
                        $sheet->cell('E'.$i, $did);   
                    }
                    if($value['tipo'] == 'FIJO')
                    {
                        $sheet->cell('E'.$i, $request['fijo']); 
                    }
                    if($value['tipo'] == 'MOVIL')
                    {
                        $sheet->cell('E'.$i, $request['movil']);
                    }
                    if($value['tipo'] == 'OTROS')
                    {
                        $sheet->cell('E'.$i, $request['otros']);  
                    }
                }  
            });
        })->export('csv');

    }

    public function exportTarifa4(Request $request)
    {
        $query = Tarifa::select(DB::raw('count(*) as total'))->get();
        foreach($query as $total)
        {
            $parte = $total->total / 4;
        }
        $parte1 = round($parte);

        $did = '0.00';

        $tarifa4 = Tarifa::skip($parte1 + $parte1 + $parte1)->take($parte1)->get();
        Excel::create('nodos_parte4_'.Carbon::now(), function($excel) use ($tarifa4, $request, $did) {
            $excel->sheet('nodos_parte4_', function($sheet) use ($tarifa4, $request, $did) {
                $sheet->cell('A1', function($cell) {$cell->setValue('Prefijo'); });
                $sheet->cell('B1', function($cell) {$cell->setValue('Description'); });
                $sheet->cell('C1', function($cell) {$cell->setValue('Intervalo N'); });
                $sheet->cell('D1', function($cell) {$cell->setValue('Intervalo 1'); });
                $sheet->cell('E1', function($cell) {$cell->setValue('Precio'); });
             
                foreach ($tarifa4 as $key => $value){
                    $i = $key+2;
                    $sheet->cell('A'.$i, $value['prefijo']); 
                    $sheet->cell('B'.$i, $value['description']); 
                    $sheet->cell('C'.$i, $value['intervalo_n']); 
                    $sheet->cell('D'.$i, $value['intervalo_n']); 
                    if($value['tipo'] == 'DID')
                    {
                        $sheet->cell('E'.$i, $did);   
                    }
                    if($value['tipo'] == 'FIJO')
                    {
                        $sheet->cell('E'.$i, $request['fijo']); 
                    }
                    if($value['tipo'] == 'MOVIL')
                    {
                        $sheet->cell('E'.$i, $request['movil']);
                    }
                    if($value['tipo'] == 'OTROS')
                    {
                        $sheet->cell('E'.$i, $request['otros']);  
                    }
                }  
            });
        })->export('csv');
    }

    public function getTarifaInternacional(Request $request)
    {
        if(Gate::allows('Root')  || Gate::allows('Tarifas'))
        {
            if (in_array("All", $request['pais'])) {
                $tarifa = DB::table('tarifa_internacional')->select(['prefijo', 'description','intervalo_n','intervalo_1','precio'])->orderBy('pais', 'asc')->get();
            }else{
                $tarifa = DB::table('tarifa_internacional')->select(['prefijo', 'description','intervalo_n','intervalo_1','precio'])->whereIn('pais',$request['pais'])->orderBy('pais', 'asc')->get();
            }
            Excel::create('nodos_internacionale_'.Carbon::now(), function($excel) use ($tarifa, $request) {
                $excel->sheet('nodos_internacionales_', function($sheet) use ($tarifa, $request) {
                    $sheet->cell('A1', function($cell) {$cell->setValue('Prefijo'); });
                    $sheet->cell('B1', function($cell) {$cell->setValue('Description'); });
                    $sheet->cell('C1', function($cell) {$cell->setValue('Intervalo N'); });
                    $sheet->cell('D1', function($cell) {$cell->setValue('Intervalo 1'); });
                    $sheet->cell('E1', function($cell) {$cell->setValue('Precio'); });
                 
                    foreach ($tarifa as $key => $value){
                        $i = $key+2;
                        $sheet->cell('A'.$i, $value->prefijo); 
                        $sheet->cell('B'.$i, $value->description); 
                        $sheet->cell('C'.$i, $value->intervalo_n); 
                        $sheet->cell('D'.$i, $value->intervalo_n);
                        if(Input::get('iva')){
                            if($request['tipo_cambio'] == "dolares"){
                                if($request['porcentaje'] != ""){
                                    $subtotal = (($value->precio * $request['porcentaje']) / 100);
                                    $total = $value->precio + $subtotal;
                                    $iva = $total * 0.16;
                                    $total = $total + $iva;
                                    $sheet->cell('E'.$i, $total); 
                                }else{
                                    $total = $value->precio + $request['suma'];
                                    $iva = $total * 0.16;
                                    $total = $total + $iva;
                                    $sheet->cell('E'.$i, $total); 
                                }
                            }else{
                                if($request['porcentaje'] != ""){
                                    $subtotal = (($value->precio * $request['porcentaje']) / 100);
                                    $total = $value->precio + $subtotal;
                                    $conversion = $total * $request['conversion'];
                                    $iva = $conversion * 0.16;
                                    $total = $conversion + $iva;
                                    $sheet->cell('E'.$i, $total); 
                                }else{
                                    $total = $value->precio + $request['suma'];
                                    $conversion = $total * $request['conversion'];
                                    $iva = $conversion * 0.16;
                                    $total = $conversion + $iva;
                                    $sheet->cell('E'.$i, $total); 
                                }
                            }
                        }else{
                            if($request['tipo_cambio'] == "dolares"){
                                if($request['porcentaje'] != ""){
                                    $subtotal = (($value->precio * $request['porcentaje']) / 100);
                                    $total = $value->precio + $subtotal;
                                    $sheet->cell('E'.$i, $total); 
                                }else{
                                    $total = $value->precio + $request['suma'];
                                    $sheet->cell('E'.$i, $total); 
                                }
                            }else{
                                if($request['porcentaje'] != ""){
                                    $subtotal = (($value->precio * $request['porcentaje']) / 100);
                                    $total = $value->precio + $subtotal;
                                    $total = $total * $request['conversion'];
                                    $sheet->cell('E'.$i, $total); 
                                }else{
                                    $total = $value->precio + $request['suma'];
                                    $total = $total * $request['conversion'];
                                    $sheet->cell('E'.$i, $total); 
                                }
                            }
                        }
                    }  
                });
            })->export('csv');
        }
    }
}
