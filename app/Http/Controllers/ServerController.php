<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\Http\Requests\ServerRequest;
use Datatables;
use Redirect;
use App\Server;
use App\Http\Repositories;
use App\Http\Repositories\ServerRepo;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;

class ServerController extends Controller
{
    protected $ServerRepo;
    
    public function __construct(ServerRepo $ServerRepo)
    {
        $this->ServerRepo = $ServerRepo;
    }

    public function index()
    { 
        if(Gate::allows('Root') || Gate::allows('Administrador') || Auth::user()->user == "m.mejia" || Auth::user()->user == "s.aceves" )
        {
          
            return view('portal.servers.servers');     
        }
        abort(404);
    }

    public function getServers(Request $request)
    {
        if($request->input('active') == 'A'){
            return DataTables::of(Server::query())->make(true);
        }else if($request->input('active') == 'Y'){
            return DataTables::of(Server::query()->whereNull('deleted_at'))->make(true);
        }else if($request->input('active') == 'N'){
            return DataTables::of(Server::query()->whereNotNull('deleted_at'))->make(true);
        }
    }

    public function getNote($id){
        $server_notes= Server::select('server_notes')->where('server_id',$id)->first();
        return $server_notes;
    }

    public function addServer()
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') || Auth::user()->user == "m.mejia" || Auth::user()->user == "s.aceves" )
        {
            return view('portal.servers.addServer');   
        }
        abort(404);
    }

    public function storeServer(Request $request)
    {
        $server = $this->ServerRepo->storeServer($request->all());
        if($server){
            Session::flash('message-success', 'Servidor agregado.');   
            return redirect()->route('servers');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al registrar el servidor');            
            return redirect()->back()->withInput(); 
        }
    }

    public function editServer($id)
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') || Auth::user()->user == "m.mejia" || Auth::user()->user == "s.aceves" )
        {
            $server = $this->ServerRepo->getServer($id);
            return View("portal.servers.editServer", compact('server')); 
        }
        abort(404);
    }

    public function updateServer(Request $request, $id)
    {
        $server = $this->ServerRepo->updateServer($request->all(), $id);
        if($server)
        {            
            Session::flash('message-success', 'Servidor actualizado.');
            return redirect()->route('servers');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al actualizar el servidor '.$id.' ');            
            return redirect()->route('servers'); 
        }
    }

    public function deleteServer($id)
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            $server = $this->ServerRepo->deleteServer($id);
            if($server)
            {
                Session::flash('message-success', 'Servidor deshabilitado.');  
                return redirect()->route('servers');
            }else{
                Session::flash('message-danger', 'Ocurrió un error al deshabilitar el servidor '.$id.'.'); 
                return redirect()->route('servers');
            }  
        }
    }

    public function online(){
        $servers = Server::select('ip')->where('active','Y')->where('provider', '!=', 'Fisico')->get();
        $serversDown = array();
        foreach($servers as $server){
            $ping = shell_exec("ping -c 1 $server->ip");
            if(!(strpos($ping, "0 received") === false)){
                $serversDown[] = $server;
            }
        }
        $respuesta = json_encode($serversDown);
        return $respuesta;
    }

}
