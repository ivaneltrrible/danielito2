<?php

namespace App\Http\Repositories;
use App\Carrier;
use Auth;
use Session;

class CarriersRepo
{
    public function storeCarrier($request)
    {
        $carrier = Carrier::create([
            'carrier' => $request['carrier'],
            'nombre' => $request['nombre'],
            'id_user' => $request['user'],
        ]);
        
        return $carrier;
    }

    public function getCarrier($id)
    {
        $carrier = Carrier::where('id_carrier', $id)->first();
        return $carrier;                  
    }

    public function updateCarrier($request, $id)
    {
        $carrier = Carrier::where('id_carrier', $id)
            ->update([
                'carrier' => $request['carrier'],
                'nombre' => $request['nombre'],
                'id_user' => $request['user'],
            ]);
        return true;
    }

    public function deleteCarrier($id)
    {
        $carrier = Carrier::where('id_carrier', $id)->delete();
        return $carrier;
    }
}