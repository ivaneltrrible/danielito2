<?php

namespace App\Http\Repositories;
use App\User;
use Auth;
use Session;

class UserRepo
{
    public function storeUser($request)
    {

        $user = User::create([
            'user' => $request['user'],
            'name' => $request['name'],
            'pass' => $request['password'],
            'password' => bcrypt($request['password']),
            'id_rol' => $request['id_rol'],
            'email' => $request['email']
        ]);
        
        return $user;
    }

    public function getUser($id)
    {
        $user = User::where('id_user', $id)->first();
        return $user;                  
    }

    public function updateUser($request, $id)
    {
        if(isset($request['password']) && $request['password'] != "" ){
            $user = User::where('id_user', $id)
            ->update([
                'user' => $request['user'],
                'name' => $request['name'],
                'id_rol' => $request['id_rol'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
            ]);
        }else{
            $user = User::where('id_user', $id)
            ->update([
                'user' => $request['user'],
                'name' => $request['name'],
                'id_rol' => $request['id_rol'],
                'email' => $request['email']
            ]);
        }
        
        return $user;
    }

    public function deleteUser($id)
    {
        $user = User::where('id_user', $id)->delete();
        return $user;
    }
}