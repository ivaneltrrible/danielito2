<?php

namespace App\Http\Repositories;
use App\Tarifa;
use Auth;
use Session;

class TarifaRepo
{
    public function storeTarifa($request)
    {
        $tarifa = Tarifa::create([
            'prefijo' => $request['prefijo'],
            'description' => $request['description'],
            'intervalo_n' => $request['intervalo_n'],
            'intervalo_1' => $request['intervalo_1'],
            'precio' => $request['precio'],
            'tipo' => $request['tipo'],
        ]);
        
        return $tarifa;
    }

    public function getTarifa($id)
    {
        $tarifa = Tarifa::where('prefijo', $id)->first();
        return $tarifa;                  
    }

    public function updateTarifa($request, $id)
    {
        $tarifa = Tarifa::where('prefijo', $id)
            ->update([
            'description' => $request['description'],
            'intervalo_n' => $request['intervalo_n'],
            'intervalo_1' => $request['intervalo_1'],
            'precio' => $request['precio'],
            ]);
        return true;
    }

    public function deleteTarifa($id)
    {
        $tarifa = Tarifa::where('prefijo', $id)->delete();
        return $tarifa;
    }
}