<?php

namespace App\Http\Repositories;
use App\Timeline;
use Auth;
use Session;
use Carbon\Carbon;

class TimeLineRepo
{
    public function storeTimeline($request,$name_archiv)
    {

        $timelin = Timeline::create([
            'id_server' => $request['id_server'],
            'id_user' => $request['id_user'],
            'comment' => $request['comment'],
            'filename' => $name_archiv,
            'status' => $request['status'],
        ]);
        
        return $timelin;
    }

    public function getServer($id)
    {
        $server = Server::where('server_id', $id)->first();
        return $server;                  
    }

    public function updateServer($request, $id)
    {
        if($request['price_provider'] == null)
            $price_provider = 0;
        else
            $price_provider = $request['price_provider'];

        if($request['price_client'] == null)
            $price_client = 0;
        else
            $price_client = $request['price_client'];

        $server = Server::where('server_id', $id)
            ->update([
                'so' => $request['so'],
                'system' => $request['system'],
                'domain' => $request['domain'],
                'ip' => $request['ip'],
                'user_ssh' => $request['user_ssh'],
                'pass_ssh' => $request['pass_ssh'],
                'port_ssh' => $request['port_ssh'],
                'provider' => $request['provider'],
                'user_web' => $request['user_web'],
                'pass_web' => $request['pass_web'],
                'client' => $request['client'],
                'reseller' => $request['reseller'],
                'customer' => $request['customer'],
                'price_provider' => $price_provider,
                'price_client' => $price_client,
                'interface' => $request['interface'],
                'active' => $request['active'],
                'server_notes'=> $request['server_notes'],

            ]);
        return $server;
    }

    public function deleteServer($id)
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $server = Server::where('server_id', $id)->update(['deleted_at' => $now]);
        return $server;
    }
}