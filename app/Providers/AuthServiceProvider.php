<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('Root', function ($user) {
            return $user->id_rol == 1;
        });

        Gate::define('Administrador', function ($user) {
            return $user->id_rol == 2;
        });

        Gate::define('Supervisor', function ($user) {
            return $user->id_rol == 3;
        });

        Gate::define('Tarifas', function ($user) {
            return $user->id_rol == 4;
        });

        Gate::define('Desarrollo', function ($user) {
            return $user->id_rol == 6;
        });
    }
}
