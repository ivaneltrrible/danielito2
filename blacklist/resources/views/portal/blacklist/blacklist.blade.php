@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-list"></i> Lista negra de la troncal: {{ $accounts->account}}</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="blacklist">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Numero Telefonico</th>
                                <th>Pais</th>
                                <th>Bloqueado</th>
                                <th><center>{!! Form::select('active', ['' => 'ACCIONES', 'Y' => 'BLOQUEAR', 'N' => 'DESBLOQUEAR', 'D' => 'ELIMINAR'], null, ['id' => 'select_camp','class' => 'form-control', 'border'=> 'none', 'onChange' => 'actionsCampaigns()']) !!}</center></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div>
		<div class="action-button-circle" data-toggle="modal">
            <a href="{{ route('blacklist/add', ['id' => $accounts->id_account]) }}" title="Agregar un numero"><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>
        </div>
	</div>

    <!-- MODAL ELIMINAR -->
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="deleteForm" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Eliminar Numero</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <!-- {{ method_field('DELETE') }} -->
                        <!-- <p class="text-center">¿Está seguro que desea eliminar?</p> -->
                        <div id="contenido"></div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()"><i class="fa fa-trash"></i> Eliminar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {

    $('#blacklist').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getBlacklist', ['id' => $accounts->id_account]) }}',
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;Excel',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'csv',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;CSV',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'pdf',
                orientation: 'landscape',
                text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;&nbsp;PDF',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;Imprimir',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'colvis',
                text: '<i class="fa fa-columns" aria-hidden="true"></i>&nbsp;&nbsp;Columnas',
                columnText: function ( dt, idx, title ) {
                    return (idx+1)+': '+title;
                }
            },
        ],
        columns: [
            {data: 'id_black', name: 'blacklist.id_black'},
            {data: 'phone_number', name: 'blacklist.phone_number'},
            {data: 'name', name: 'countrys.name'},
            // {data: 'blocked', name: 'blacklist.blocked'},
            {"render": function(data, type, row) {
                if(row.blocked == 'y')
                {
                    return "<center><a href='blacklist/blocked/" + row.id_black +"'title=\"Bloquer/Desbloquear\"><span style='color:red'>BLOQUEADO</span></a></center>";
                }
                else
                {
                    return "<center><a href='blacklist/blocked/" + row.id_black +"'title=\"Bloquer/Desbloquear\"><span>DESBLOQUEADO</span></a></center>";
                }
            },
            "targets": 3},
            {"render": function(data, type, row) {
                return "<center><input name='checkBlack[]' type='checkbox' class='blacklistNumbers' value='"+row.id_black+"'>&nbsp;&nbsp;&nbsp;&nbsp;\
                <a href='blacklist/edit/" + row.id_black + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
                <a href='javascript:;' data-toggle='modal' onclick='deleteBlacklist(\""+row.id_black+"\",\""+row.phone_number+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></center>";
            },
            "targets": 4,
            "orderable": false}
        ]
    });
});

function deleteBlacklist(id,phone)
{
    var id = id;
    var url = '{{ route("blacklist/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append(" <p class='text-center'>¿Está seguro que desea eliminar el usuario "+phone+"?</p>");
}
function changeBlocked($id){
    var id = id;
    var url = '{{ route("blacklist/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append(" <p class='text-center'>¿Está seguro que desea eliminar el usuario "+phone+"?</p>");
}

function formSubmit()
{
    $("#deleteForm").submit();
}

function actionsCampaigns()
{
    var numeros = [];
    $("input[type=checkbox]:checked").each(function(){
        numeros.push(this.value);
    });
    
    var option = $('#select_camp').val();
    var token = '{{ csrf_token() }}';
    var data = {'_token':token, 'numeros':numeros};
    if (numeros.length<1)
    {
        alert('Selecciona un numero.');
        $("#select_camp").val("")
    }
    else
    {
        if (option == 'Y' )
        {
            console.log(numeros);
            var r = confirm("¿Estás seguro que quieres bloquear, esto(s) numero(s)?");
            if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{route('blacklist/number/bloquear')}}",
                data: data,
                dataType: "json",
                success: function(data){
                    alert("Numero(s) bloqueada(s)");
                    location.reload();                                                          
                },
                error: function(data){
                    console.log(data);
                    alert("Hubo un error");
                    location.reload();
                }
            }); 
            }
        }
        else if (option == 'N' )
        {
            console.log(numeros);
            var r = confirm("¿Estás seguro que quieres desbloquear, esto(s) numero(s)?");
            if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{route('blacklist/number/desbloquear')}}",
                data: data,
                dataType: "json",
                success: function(data){
                    alert("Numero(s) desbloqueada(s)");
                    location.reload();                                                          
                },
                error: function(data){
                    console.log(data);
                    alert("Hubo un error");
                    location.reload();
                }
            }); 
            }
        }else if (option == 'D' )
        {
            console.log(numeros);
            var r = confirm("¿Estás seguro que quieres eliminar, esto(s) numero(s)?");
            if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{route('blacklist/number/delete')}}",
                data: data,
                dataType: "json",
                success: function(data){
                    alert("Numero(s) eliminados(s)");
                    location.reload();                                                          
                },
                error: function(data){
                    console.log(data);
                    alert("Hubo un error");
                    location.reload();
                }
            }); 
            }
        }
    }
        
}

</script>
@endsection



