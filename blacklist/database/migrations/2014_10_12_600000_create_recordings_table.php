<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recordings', function (Blueprint $table) {
            $table->increments('id_record');
            $table->integer('id_cdr');
            $table->integer('id_server');
            $table->datetime('call_date');
            $table->string('dst');
            $table->string('duration');
            $table->string('recording_file');
            $table->string('recording_local');
            $table->string('cnum');
            $table->string('cname');
            $table->string('uniqueid');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recordings');
    }
}