<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id_server');
            $table->string('ip');
            $table->string('user');
            $table->string('pass');
            $table->string('bd');
            $table->string('user_bd');
            $table->string('pass_bd');
            $table->string('client');
            $table->string('host');
            $table->string('res_bd_1');
            $table->string('res_bd_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
