<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'servers';
    public $timestamps = false;
    protected $primaryKey = 'id_server';
    protected $fillable = [
        'id_server',
        'ip',
        'user',
        'pass',
        'bd',
        'user_bd',
        'pass_bd',
        'client',
        'host',
        'res_bd_1',
        'rest_bd_2',
        'ssh_key',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $visible = [
        'id_server',
        'ip',
        'user',
        'pass',
        'bd',
        'user_bd',
        'pass_bd',
        'client',
        'host',
        'res_bd_1',
        'rest_bd_2',
        'ssh_key',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [

    ];
}