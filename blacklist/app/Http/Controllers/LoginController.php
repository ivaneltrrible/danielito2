<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function getLogin(){
        return view('authentication.index');
    }

    public function postLogin(LoginRequest $request){
        if(Auth::attempt(['user' => $request['usuario'], 'password' => $request['contraseña'] ],$request['remember']))
        {
            // Session::flash('message-danger', $auth);
            return Redirect::to('accounts');
        }
        Session::flash('message-danger', 'Los datos son incorrectos');
        return Redirect::to('/');
    }

    public function getLogout()
    {
        Auth::logout();
        return Redirect::to('/');
    }
}
