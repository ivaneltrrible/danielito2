<?php

namespace App\Http\Controllers;

use App\AdminLogs;
use App\Http\Controllers\Controller;
use App\Http\Repositories\AccountsRepo;
use App\Http\Requests\AccountsRequest;
use App\User;
use Auth;
use Carbon\Carbon;
use Datatables;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;

class AccountsController extends Controller
{
    protected $AccountsRepo;

    public function __construct(AccountsRepo $AccountsRepo)
    {
        $this->AccountsRepo = $AccountsRepo;

    }

    public function index()
    {
        return view('portal.accounts.accounts');
    }

    public function getAccounts()
    {

        $accounts = DB::table('accounts')->join('users', 'users.id_user', '=', 'accounts.id_user')->select('users.user', 'accounts.id_account', 'accounts.account', 'accounts.description');
        return DataTables::of($accounts)->make(true);

    }

    public function addAccounts()
    {
        $users = DB::table('users')->get();
        $users_list = array();
        foreach ($users as $user) {
            $users_list[$user->id_user] = $user->user;
        }
        return view('portal.accounts.addAccounts', compact('users_list'));
        //var_dump($roles_list);

    }

    public function storeAccounts(AccountsRequest $request)
    {
        //CONSULTA PARA VALIDAR QUE TRONCAL TIENE ANTES DE CREAR
        // $accounts_anterior = $this->AccountsRepo->getAccounts($id);
        $accounts = $this->AccountsRepo->storeAccounts($request->all());
        if ($accounts[0]) {
            //se crea log
            AdminLogs::create([
                'event_date' => Carbon::now(),
                'user' => Auth::user()->user,
                'event_section' => 'TRONCALES',
                'event_type' => 'AGREGAR',
                'event_sql' => 'El usuario ' . Auth::user()->user . ' agrego el troncal ' . $accounts[0]->account . ' al sistema.',
                'query' => $accounts[1],
            ]);
            Session::flash('message-success', 'Account ' . $accounts[0]->account . ' agregado.');
            return redirect()->route('accounts');

        } else {
            Session::flash('message-danger', 'Ocurrió un error al registrar la cuenta');
            return redirect()->back()->withInput();
        }
    }

    public function editAccounts($id)
    {

        $accounts = $this->AccountsRepo->getAccounts($id);
        $users = DB::table('users')->get();
        $users_list = array();
        foreach ($users as $user) {
            $users_list[$user->id_user] = $user->user;
        }
        return View("portal.accounts.editAccounts", compact('accounts', 'users_list'));

    }

    public function updateAccounts(Request $request, $id)
    {
        //CONSULTA PARA VALIDAR QUE TRONCAL TIENE ANTES DE ACTUALIZAR
        $accounts_anterior = $this->AccountsRepo->getAccounts($id);
        
        $accounts = $this->AccountsRepo->updateAccounts($request->all(), $id);
        if ($accounts[0]) {
            //se crea log
            AdminLogs::create([
                'event_date' => Carbon::now(),
                'user' => Auth::user()->user,
                'event_section' => 'TRONCALES',
                'event_type' => 'ACTUALIZACION',
                'event_sql' => 'El usuario ' . Auth::user()->user . ' actualizo el troncal ' . $accounts_anterior->account . ' a ' . $request['account'],
                'query' => $accounts[1],
            ]);

            Session::flash('message-success', 'Troncal actualizada.');
            return redirect()->route('accounts');
        } else {
            Session::flash('message-danger', 'Ocurrió un error al actualizar la troncal ' . $id . ' ');
            return redirect()->back()->withInput();
        }
    }

    public function deleteAccounts($id)
    {
        //CONSULTA PARA VALIDAR QUE TRONCAL QUE TIENE ANTES DE ELIMINAR
        $accounts_eliminado = $this->AccountsRepo->getAccounts($id);
        $accounts = $this->AccountsRepo->deleteAccounts($id);
        if ($accounts[0]) {
            //se crea log
            AdminLogs::create([
                'event_date' => Carbon::now(),
                'user' => Auth::user()->user,
                'event_section' => 'TRONCALES',
                'event_type' => 'ELIMINAR',
                'event_sql' => 'El usuario ' . Auth::user()->user . ' Elimino el troncal ' . '<b>'.  $accounts_eliminado->account . '</b>' . ' del sistema.',
                'query' => $accounts[1],
            ]);

            Session::flash('message-success', 'Troncal Eliminada Correctamente.');
            return redirect()->route('accounts');
        } else {
            return redirect()->back()->withInput();
        }

    }

    public function getCentralesUser($id)
    {
        $central = Central::where('id_customer', $id)->get();
        return $central;
    }
}
