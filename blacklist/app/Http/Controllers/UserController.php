<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Datatables;
use Redirect;
use App\User;
use App\Http\Repositories;
use App\Http\Repositories\UserRepo;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;
use App\AdminLogs;

class UserController extends Controller
{
    protected $UserRepo;
    
    public function __construct(UserRepo $UserRepo)
    {
        $this->UserRepo = $UserRepo;
        
    }

    public function index()
    {     
        return view('portal.user.users');     
    }

    public function getUsers()
    {
        
        return DataTables::of(User::where('id_user','!=','0' ))->make(true);
    }

    public function addUser()
    {
        $roles = DB::table('roles')->get();
        $roles_list = array();
        foreach ($roles as $rol) {
            $roles_list[$rol->id_rol] = $rol->name;
        }
        return view('portal.user.addUser', compact('roles_list')); 
        //var_dump($roles_list);
    }

    public function storeUser(UserRequest $request)
    {
        $user = $this->UserRepo->storeUser($request->all());
        if($user[0]){

            AdminLogs::create([
                'event_date' => Carbon::now(),
                'user' => Auth::user()->user,
                'event_section' => 'USUARIOS',
                'event_type' => 'AGREGAR',
                'event_sql' => 'El admin ' . Auth::user()->user . ' agrego al usuario ' . $user[0]->user . ' al sistema.',
                'query' => $user[1],
            ]);
            Session::flash('message-success', 'Usuario '.$user[0]->user.' agregado.');   
            return redirect()->route('users');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al registrar el usuario');            
            return redirect()->back()->withInput(); 
        }
    }

    public function editUser($id)
    {
        
            
        $user = $this->UserRepo->getUser($id);
        $roles = DB::table('roles')->get();
        $roles_list = array();
        foreach ($roles as $rol) {
            $roles_list[$rol->id_rol] = $rol->name;
        }
        return View("portal.user.editUser", compact('user', 'roles_list')); 
    }

    public function updateUser(Request $request, $id)
    {
          //CONSULTA PARA VALIDAR QUE usuario TIENE ANTES DE ACTUALIZAR
        $user_anterior = $this->UserRepo->getUser($id);

        $user = $this->UserRepo->updateUser($request->all(), $id);
        if($user[0])
        { 
            //CONSULTA PARA EL USUARIO ACTUAL
            $user_actual = $this->UserRepo->getUser($id);
            AdminLogs::create([
                'event_date' => Carbon::now(),
                'user' => Auth::user()->user,
                'event_section' => 'USUARIOS',
                'event_type' => 'ACTUALIZAR',
                'event_sql' => 'El admin ' . Auth::user()->user . ' actualizo al usuario ' . $user_anterior->user . ' a '. $user_actual->user,
                'query' => $user[1],
            ]);           
            Session::flash('message-success', 'Usuario actualizado.');
            return redirect()->route('users');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al actualizar el usuario '.$id.' ');            
            return redirect()->back()->withInput(); 
        }
    }

    public function deleteUser($id)
    {
        {
           
            //CONSULTA PARA VALIDAR QUE TRONCAL QUE TIENE ANTES DE ELIMINAR
            $user_eliminado = $this->UserRepo->getUser($id);

            $user = $this->UserRepo->deleteUser($id);
            if($user[0])
            {
                AdminLogs::create([
                    'event_date' => Carbon::now(),
                    'user' => Auth::user()->user,
                    'event_section' => 'USUARIOS',
                    'event_type' => 'ELIMINAR',
                    'event_sql' => 'El admin ' . Auth::user()->user . ' Elimino al usuario ' . '<b>'.  $user_eliminado->user . '</b>' . ' del sistema.',
                    'query' => $user[1],
                ]);

                Session::flash('message-success', 'Usuario eliminado.');  
                return redirect()->route('users');
            }else{
                return redirect()->back()->withInput(); 
            }  
        }
    }

    public function getCentralesUser($id)
    {
        $central = Central::where('id_customer', $id)->get();
        return $central;
    }
}
