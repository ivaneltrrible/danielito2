<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
Use Auth;
use App\AdminLogs;
use Datatables;
use Mail;
use App\Providers\AuthServiceProvider;

use Gate;

class LogsController extends Controller
{
    public function index(){
      
    }

    public function logs(){
  
       
         if(Gate::Allows('Administrador')){
             return view('portal.logs.logs');
         }else{
             return view('portal.logs.logsUsuario');
         }
        
    }
    

    public function getLogs(){
        return DataTables::of(AdminLogs::query())->make(true);
    }

    public function getLogsUsuario(){
        return DataTables::of(AdminLogs::select('id_log', 'event_date', 'user', 'event_section', 'event_type', 'event_sql')->where('user', Auth::user()->user ))->make(true);
    }

    public function sendEmailHDD($para, $asunto, $de, $nombre, $mensaje){
        $this->para = $para;
        $this->asunto = $asunto;
        $this->de = $de;
        $this->nombre = $nombre;

        $data = ['mensaje' => $mensaje];
        $mail = Mail::send('root.email.template', $data, function ($message) use ($request) {
            $corp = $this->$nombre;
            $from = $this->$de;

            $to = $this->$para;
            $subject = $this->$asunto;
            
            $message->from($from, $corp);
            $message->to($to)->subject($subject);
        });
    }
}
