<?php

namespace App\Http\Repositories;
use App\Blacklist;
use Auth;
use Session;
use DB;

class BlacklistRepo
{
    public function storeBlacklist($request)
    {
        //SE HABILATA EL QUERY
        DB::enableQueryLog();

        //Crear una cuenta en la base de datos 
        $blacklist = Blacklist::create([
            'phone_number' => $request['phone_number'],
            'country' => $request['country'],
            'id_account' => $request['id_account'],
            'blocked' => $request['blocked']
        ]);

         //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        // $query = (DB::getQueryLog()[0]['query']);
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_blacklist = array($blacklist, $consulta);
        
        return($arreglo_blacklist);

    }

    public function getBlacklist($id)
    {
        $blacklist = Blacklist::where('id_black', $id)->first();
        return $blacklist;                  
    }

    public function repeatBlacklist($number, $account)
    {
        $blacklist = Blacklist::where('phone_number', $number)->where('id_account', $account)->first();
        return $blacklist;   
    }

    public function updateBlacklist($request, $id)
    {
        //SE HABILATA EL QUERY
        DB::enableQueryLog();

        $blacklist = Blacklist::where('id_black', $id)
            ->update([
                'phone_number' => $request['phone_number'],
                'country' => $request['country'],
                'blocked' => $request['blocked']
            ]);

            //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        // $query = (DB::getQueryLog()[0]['query']);
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_blacklist = array($blacklist, $consulta);
        
        return($arreglo_blacklist);
       
    }

    public function deleteBlacklist($id)
    {
        //SE HABILATA EL QUERY
        DB::enableQueryLog();

        $blacklist = Blacklist::where('id_black', $id)->delete();

        //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_blacklist = array($blacklist, $consulta);
        
        return($arreglo_blacklist);
    }

    public function _blockedBlacklist($id)
    {
        $blacklist = Blacklist::where('id_black', $id)->first();
        if($blacklist->blocked == 'n')
        {
            $blacklist->update(['blocked' => 'y']);
        }
        else
        {
            $blacklist->update(['blocked' => 'n']);
        }
        return $blacklist;
    }
}