<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //Son reglas que tiene que cumplir el usuario(no permite campo vacio, que no se repita, y maximo 20 caracteres)
            'account' => 'required|unique:accounts|max:20',
            'description' => 'required|max:150',
            'id_user' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'account.required' => 'El campo "Account" es requerido.',
            'account.unique' => 'El campo "Troncal" ya existe.',
            'account.max' => 'El campo "Account" debe tener 20 caracteres como máximo.',
            'description.required' => 'El campo "Descripcion" es requerido.',
            'description.max' => 'El campo "Descripcion" debe tener 150 caracteres como máximo.',
            'id_user.required' => 'El campo "Usuario" es requerido.'
        ];
    }
}
